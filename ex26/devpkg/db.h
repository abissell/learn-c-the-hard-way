#ifndef _db_h
#define _db_h

#define DB_FILE "/usr/local/.devpkg/db"
#define DB_DIR "/usr/local/.devpkg"
#define MAX_LOADS 10
#define DB_SIZE_GUESS 5000

int DB_init();
int DB_list();
int DB_update(const char *url);
int DB_find(const char *url);

#endif
