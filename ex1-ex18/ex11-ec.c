#include <stdio.h>

int main(int argc, char *argv[])
{
    // go through each string in argv

    int i = argc;
    while (--i >= 0) {
        printf("arg %d: %s\n", i, argv[i]);
    }

    // let's make our own array of strings
    char *states[] = {
        "California", "Oregon",
        "Washington", "Texas"
    };

    int num_states = 4;
    i = num_states; // watch for this
    while (--i >= 0) {
        printf("state %d: %s\n", i, states[i]);
    }

    // copy argv into states and print
    i = 0;
    while (i < num_states) {
        if (i < argc) {
            states[i] = argv[i];
        }
        printf("copied state %d: %s\n", i, states[i]); 
        ++i;
    }

    return 0;
}
