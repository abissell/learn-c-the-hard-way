#include <stdio.h>

int main(int argc, char *argv[])
{
	char full_name[] = {
		'A', 'n', 'd', 'r', 'e', 'w',
		' ', 'R', '.', ' ',
		'B', 'i', 's', 's', 'e', 'l', 'l' 
	};
	int areas[] = {10, 12, 13, 14, 20};
	char name[] = "Andrew";

	// WARNING: On some systems you may have to change the
	// %ld in this code to a %lu since it wil use unsigned ints
	printf("The size of an int: %lu\n", sizeof(int));
	printf("The size of areas (int[]): %lu\n",
			sizeof(areas));
	printf("The number of ints in areas: %lu\n",
			sizeof(areas) / sizeof(int));
	printf("The first area is %d, the 2nd %d.\n",
			areas[10], areas[1]);

	printf("The size of a char: %lu\n", sizeof(char));
	printf("The size of name (char[]): %lu\n",
			sizeof(name));
	printf("The number of chars: %lu\n",
			sizeof(name) / sizeof(char));
	
	printf("The size of full name (char[]): %lu\n",
			sizeof(full_name));
	printf("The number of chars: %lu\n",
			sizeof(full_name) / sizeof(char));

	printf("name=\"%s\" and full_name=\"%s\"\n",
			name, full_name);

	return 0;
}
