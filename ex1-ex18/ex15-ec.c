#include <stdio.h>

void print_with_indexing(int count, int *ages_ptr, char **names_ptr);
void print_with_pointers(int count, int *ages_ptr, char **names_ptr);
void print_years_alive(int age, char *name);

int main(int argc, char *argv[])
{
    // create two arrays we care about
    int ages[] = {23, 43, 12, 89, 2};
    char *names[] = {
       "Alan", "Frank",
       "Mary", "John", "Lisa"
    };

    // safely get the size of ages
    int count = sizeof(ages) / sizeof(int);
    int i = 0;

    // first way using indexing
    print_with_indexing(count, ages, names);

    printf("---\n");

    // rewritten using pointers instead of arrays
    int *ages_ptr = &ages[0];
    char **names_ptr = &names[0]; 

    // try printing using the pointers
    int ptr_count = sizeof(ages) / sizeof(int);
    int j = 0;

    // using indexing
    for (j = 0; j < ptr_count; ++j) {
        printf("%s has %d years alive.\n",
                *(names_ptr+j), *(ages_ptr+j));
    }

    printf("---\n");

    // set up the pointers to the start of the arrays
    int *cur_age = ages;
    char **cur_name = names;

    // second way using pointers
    print_with_indexing(count, cur_age, cur_name);

    printf("---\n");

    // third way, pointers are just arrays
    for (i = 0; i < count; ++i) {
        printf("%s is %d years old again.\n",
                cur_name[i], cur_age[i]);
    }

    printf("---\n");

    // fourth way with pointers in a stupid complex way
    for (cur_name = names, cur_age = ages;
             (cur_age - ages) < count;
             cur_name++, cur_age++) {
        printf("%s lived %d years so far.\n",
                *cur_name, *cur_age);
    }

    printf("---\n");

    // reset pointers to start of arrays and print addresses
    cur_age = ages;
    cur_name = names;
    for (i = 0; i < count; ++i) {
        printf("address of %dth position ages: %p, names %p\n", 
                i, &(*(cur_age+i)), &(*(cur_name+i)));
    }

    printf("---\n");

    // same loop again, perhaps showing no effect from &*
    for (i = 0; i < count; ++i) {
        printf("address of %dth position ages: %p, names %p\n",
                i, cur_age+i, cur_name+i);
    }

    printf("---\n");

    // same loop again, with address of array index rather than pointer arithmetic
    for (i = 0; i < count; ++i) {
        printf("address of %dth position ages: %p, names %p\n",
                i, &cur_age[i], &cur_name[i]);
    }

    return 0;
} 

void print_with_indexing(int count, int *ages_ptr, char **names_ptr)
{
    int i = 0;
    for (i = 0; i < count; ++i) {
        print_years_alive(ages_ptr[i], names_ptr[i]);
    }
}

void print_with_pointers(int count, int *ages_ptr, char **names_ptr)
{
    int i = 0;
    for (i = 0; i < count; ++i) {
        print_years_alive(*(ages_ptr+i), *(names_ptr+i));
    }
}

void print_years_alive(int age, char *name)
{
    printf("%s is %d years old.\n", name, age);
}
