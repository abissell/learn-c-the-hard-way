#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    int i = 0;

    char *p;
    int pi;
    p = malloc(sizeof(char));
    p[0] = 'a';
    pi = 0;

    while (i < 100) {
        usleep(3000);
        ++pi;

        if (p[0] == '\0') break;

        if (pi % 20 == 17) {
            free(p);
            p[0] = '4';
        }
    }

    return 0;
}
