#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include "object.h"
#include "game.h"

Object MonsterProto = {
    .init = Monster_init,
    .attack = Monster_attack
};

Object RoomProto = {
    .move = Room_move,
    .attack = Room_attack
};

int Map_init(void *self)
{
    assert(self != NULL);

    Map *map = self;

    // make some rooms for a different map
    Room *hall = NEW(Room, "The hall");
    assert(hall != NULL);
    Room *billiards = NEW(Room, "The billiard room");
    assert(billiards != NULL);
    Room *study = NEW(Room, "The study");
    assert(study != NULL);
    Room *kitchen = NEW(Room, "The kitchen, with Professor Plum");
    assert(kitchen != NULL);
    Room *conservatory = NEW(Room, "The conservatory");

    // put Prof. Plum in the kitchen
    kitchen->bad_guy = NEW(Monster, "Professor Plum");
    assert(kitchen->bad_guy != NULL);

    // setup the map rooms
    hall->north = kitchen;
    hall->west = billiards;
    hall->east = study;

    billiards->east = hall;
    billiards->south = conservatory;

    conservatory->north = billiards;

    kitchen->south = hall;

    study->west = hall;

    // start the map and the character off in the hall
    map->start = hall;
    map->location = hall;

    return 1;
}

Object MapProto = {
    .init = Map_init,
    .move = Map_move,
    .attack = Map_attack
};

int main(int argc, char *argv[])
{
    // simple way to setup the randomness
    srand(time(NULL));

    // make our map to work with
    Map *game = NEW(Map, "The game of Clue.");

    printf("You enter the ");
    game->location->_(describe)(game->location);

    while(process_input(game)) {
    }

    return 0;
}
