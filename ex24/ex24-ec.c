#include <stdio.h>
#include "../dbg.h"

#define MAX_DATA 100

typedef enum EyeColor {
    BLUE_EYES, GREEN_EYES, BROWN_EYES,
    BLACK_EYES, OTHER_EYES
} EyeColor;

const char *EYE_COLOR_NAMES[] = {
    "Blue", "Green", "Brown", "Black", "Other"
};

typedef struct Person {
    int age;
    char first_name[MAX_DATA];
    char last_name[MAX_DATA];
    EyeColor eyes;
    float income;
} Person;

void clean_name(char *name_ptr)
{
    int i = 0;
    for (i = 0; i < MAX_DATA - 1; i++) {
        char c = *name_ptr;
        if (c == '\0') {
            break;
        } else if (c == ' ' || c == '\n') {
            *name_ptr = '\0';
            break;
        }
        name_ptr++;
    }
}

void read_indiv_chars(char *name_ptr, int len)
{
    for (int i = 0; i < len-1; i++) {
        char *in = NULL;
        int rc = scanf("%c", name_ptr);
        check(rc > 0, "Failed to read char.");
        if (*name_ptr == '\0') {
            break;
        } else if (*name_ptr == '\n') {
            *name_ptr = '\0';
            break;
        } else {
            name_ptr++;
        }
    }

    if (*name_ptr != '\0') {
        *name_ptr = '\0';
    }

    error:
        return;
}

int main(int argc, char *argv[])
{
    Person you = {.age = 0};
    int i = 0;
    char *in = NULL;

    printf("What's your First Name? ");
    read_indiv_chars(you.first_name, MAX_DATA);
    // in = fgets(you.first_name, MAX_DATA-1, stdin);
    // check(in != NULL, "Failed to read first name.");
    clean_name(you.first_name);

    printf("What's your Last Name? ");
    in = fgets(you.last_name, MAX_DATA-1, stdin);
    check(in != NULL, "Failed to read last name.");
    clean_name(you.last_name);

    printf("How old are you? ");
    int rc = scanf("%d", &you.age);
    // int rc = fscanf(stdin, "%d", &you.age);
    check(rc > 0, "You have to enter a valid number.");
    /*
    char in_buf[50];

    in = fgets(in_buf, 50 - 1, stdin);
    check(in != NULL, "Failed to read age.");
    int age = atoi(in_buf);
    you.age = age;
    */
    printf("What color are your eyes:\n");
    for (i = 0; i <= OTHER_EYES; i++) {
        printf("%d) %s\n", i+1, EYE_COLOR_NAMES[i]);
    }
    printf("> ");

    int eyes = -1;
    /*
    in = fgets(in_buf, 50 - 1, stdin);
    check(in != NULL, "Failed to read eye color.");
    eyes = atoi(in_buf);
    */
    rc = scanf("%d", &eyes);
    // rc = fscanf(stdin, "%d", &eyes);
    check(rc > 0, "You have to enter a number.");

    you.eyes = eyes - 1;
    check(you.eyes <= OTHER_EYES && you.eyes >= 0, "Do it right, that's not an option.");

    printf("How much do you make an hour? ");
    /*
    in = fgets(in_buf, 50 - 1, stdin);
    check(in != NULL, "Failed to read income.");
    you.income = atoi(in_buf);
    */
    rc = scanf("%f", &you.income);
    // rc = fscanf(stdin, "%f", &you.income);
    check(rc > 0, "Enter a floating point number.");

    printf("----- RESULTS -----\n");

    printf("First Name: %s\n", you.first_name);
    printf("Last Name: %s\n", you.last_name);
    printf("Age: %d\n", you.age);
    printf("Eyes: %s\n", EYE_COLOR_NAMES[you.eyes]);
    printf("Income: %f\n", you.income);

    return 0;
error:

    return -1;
}
